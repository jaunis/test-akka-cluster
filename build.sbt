name := "test-akka-cluster"

scalaVersion := "2.11.1"

version := "1.0"

scalacOptions += "-target:jvm-1.7"

lazy val root = (project in file(".")).enablePlugins(play.PlayScala)

libraryDependencies ++= Seq(jdbc,
                            anorm,
                            cache,
                            ws,
                            "com.typesafe.akka" %% "akka-cluster" % "2.4-M2",
                            "com.typesafe.akka" %% "akka-cluster-tools" % "2.4-M2")

