import akka.actor.{Props, ActorSystem}
import cluster.Test
import org.slf4j.LoggerFactory
import play.api.{Application, GlobalSettings}


object Global extends GlobalSettings {

  val logger = LoggerFactory.getLogger(getClass)

  override def onStart(app: Application) {
    val system = ActorSystem("ClusterSystem")
    val ref = system.actorOf(Props(new Test), "testactor")
    logger.error(s"Created the actor ${ref.path}")
  }

}
