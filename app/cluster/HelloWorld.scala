package cluster

import akka.actor.ActorRef

case class HelloWorld(sender: ActorRef)
