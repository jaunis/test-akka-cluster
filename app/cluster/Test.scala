package cluster

import akka.actor.{Actor, ActorLogging}
import akka.cluster.Cluster
import akka.cluster.ClusterEvent._
import akka.cluster.pubsub.DistributedPubSubMediator.{Put, SendToAll}
import akka.cluster.pubsub.DistributedPubSub

class Test extends Actor with ActorLogging {

  val mediator = DistributedPubSub(context.system).mediator
  mediator ! Put(self)
  val cluster = Cluster(context.system)

  override def preStart(): Unit = {
    cluster.subscribe(self, initialStateMode = InitialStateAsEvents, classOf[MemberEvent])
  }

  def receive = {
    case MemberUp(member) => log.error("Member is Up: {}", member.address)
      mediator ! SendToAll("/user/testactor", HelloWorld(self))
    case HelloWorld(ref) => log.error(s"Received HelloWorld from $sender, but coming really from $ref")
    case e => log.error(s"Received unknown event $e")
  }
}

